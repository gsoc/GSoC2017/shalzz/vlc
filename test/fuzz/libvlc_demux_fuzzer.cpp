/*****************************************************************************
 * libvlc_demux_fuzzer.cpp: fuzz target for demux modules
 *****************************************************************************
 * Copyright (C) 2017 VideoLAN
 *
 * Authors: Shaleen Jain <shaleen.jain95@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

/*
 * Fuzz the various demux modules.
 *
 * The avformat demux module can be disabled since its
 * upstream project ffmpeg is already fuzz tested.
 *
 * libvlc_demux_fuzzer --disable-avformat
 */

#include <vlc/libvlc.h>

#include <vlc_common.h>
#include <vlc_stream.h>
#include <vlc_demux.h>
#include <vlc_meta.h>
#include <vlc_modules.h>
#include <assert.h>

#include "../../lib/libvlc_internal.h"

extern "C" {
#include "fuzzer.h"
}

static libvlc_instance_t *p_libvlc;

struct test_es_out_t
{
    struct es_out_t out;
    struct es_out_id_t *ids;
};

struct es_out_id_t
{
    struct es_out_id_t *next;
};

int FuzzerInitialize(int *argc, char ***argv) {

    setenv("VLC_PLUGIN_PATH", "../../modules", 1);
    p_libvlc = libvlc_new(0, NULL);
    assert(p_libvlc != NULL);

    /* Disable all logging for increased performance*/
    libvlc_log_unset(p_libvlc);
    return 0;
}

int pf_send_stub ( es_out_t *out, es_out_id_t *es, block_t *p_block ) {
    block_Release( p_block );
    return VLC_SUCCESS;
}

es_out_id_t * pf_add_stub( es_out_t *out, const es_format_t *fmt ) {
    struct test_es_out_t *test_out = (struct test_es_out_t *) out;

    if (fmt->i_group < 0)
        return NULL;

    es_out_id_t *id = (es_out_id_t *)malloc(sizeof (*id));
    if (unlikely(id == NULL))
        return NULL;

    id->next = test_out->ids;
    test_out->ids = id;

    return id;
}
void pf_del_stub( es_out_t *out, es_out_id_t *id ) {
    free( id );
}

int pf_control_stub ( es_out_t *out, int i_query, va_list args ) { return VLC_SUCCESS; }

void pf_destroy_stub ( es_out_t *out ) {
    struct test_es_out_t *test_out = (struct test_es_out_t *)out;
    es_out_id_t *id;

    while ((id = test_out->ids) != NULL)
    {
        test_out->ids = id->next;
        free(id);
    }
    free(test_out);
}

static es_out_t *create_test_es_out(vlc_object_t *parent) {
    struct test_es_out_t *test_out = (test_es_out_t *) malloc(sizeof (*test_out));
    if (test_out == NULL)
    {
        return NULL;
    }

    test_out->ids = NULL;

    es_out_t *out = &test_out->out;
    out->pf_add = pf_add_stub;
    out->pf_send = pf_send_stub;
    out->pf_del = pf_del_stub;
    out->pf_control = pf_control_stub;
    out->pf_destroy = pf_destroy_stub;
    out->p_sys = (es_out_sys_t *)parent;

    return out;
}

unsigned demux_TestAndClearFlags( demux_t *demux, unsigned flags )
{
    unsigned i_update;
    if ( demux_Control( demux, DEMUX_TEST_AND_CLEAR_FLAGS, &i_update ) == VLC_SUCCESS )
        return i_update;
    unsigned ret = demux->info.i_update & flags;
    demux->info.i_update &= ~flags;
    return ret;
}

void demux_GetMeta(demux_t *demux) {
    vlc_meta_t *p_meta = vlc_meta_New();
    if( unlikely(p_meta == NULL) )
        return;

    input_attachment_t **attachment;
    int i_attachment;

    demux_Control( demux, DEMUX_GET_META, p_meta );
    demux_Control( demux, DEMUX_GET_ATTACHMENTS, &attachment, &i_attachment );

    vlc_meta_Delete( p_meta );
}

int FuzzerTestOneInput(const uint8_t *Data, size_t Size) {

    // Create an input stream
    stream_t *stream = vlc_stream_MemoryNew(p_libvlc->p_libvlc_int,
            const_cast<uint8_t *>(Data), Size, true);
    assert(stream != NULL);

    es_out_t *es_out = create_test_es_out(VLC_OBJECT(stream));
    if(es_out == NULL)
        return 0;

    demux_t *demux = demux_New(VLC_OBJECT(p_libvlc->p_libvlc_int), "","", stream, es_out);
    if(demux == NULL)
    {
        es_out_Delete(es_out);
        vlc_stream_Delete(stream);
        return 0;
    }

    /* Do not fuzz avformat(ffmpeg) demux*/
    if( disable_avformat && strcmp(module_get_name(demux->p_module, false), "Avformat") )
    {
        es_out_Delete(es_out);
        demux_Delete( demux );
        return 0;
    }

    int i_ret;
    do {
        i_ret = demux_Demux( demux );

        if( demux_TestAndClearFlags( demux, INPUT_UPDATE_TITLE_LIST ) )
            demux_Control( demux, DEMUX_GET_TITLE_INFO );

        if( demux_TestAndClearFlags( demux, INPUT_UPDATE_META ) )
            demux_GetMeta( demux );

        int seekpoint = 0;
        double position = 0.0;
        mtime_t time = 0;
        mtime_t length = 0;

        /* Call controls for increased code coverage */
        demux_Control( demux, DEMUX_GET_SEEKPOINT, &seekpoint );
        demux_Control( demux, DEMUX_GET_POSITION, &position );
        demux_Control( demux, DEMUX_GET_TIME, &time );
        demux_Control( demux, DEMUX_GET_LENGTH, &length );

    } while( i_ret == VLC_DEMUXER_SUCCESS );

    demux_Delete( demux );
    es_out_Delete(es_out);
    return 0;
}

void FuzzerCleanup(void) {
    libvlc_release(p_libvlc);
}
