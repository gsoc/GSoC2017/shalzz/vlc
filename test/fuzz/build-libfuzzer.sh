#!/bin/bash
SCRIPT_DIR="$(cd "$(dirname $0)" && pwd)"
BUILD_DIR=/tmp
LIBFUZZER_SRC=$BUILD_DIR/Fuzzer

get_libfuzzer() {
    git clone https://chromium.googlesource.com/chromium/llvm-project/llvm/lib/Fuzzer $LIBFUZZER_SRC ||
    (cd $LIBFUZZER_SRC && git pull)
}

get_libfuzzer &&
cd $LIBFUZZER_SRC && ./build.sh &&
cp $LIBFUZZER_SRC/libFuzzer.a $SCRIPT_DIR/
