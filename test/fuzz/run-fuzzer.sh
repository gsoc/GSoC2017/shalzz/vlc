jobs=1

./libvlc_demux_fuzzer -jobs=$jobs DEMUX_CORPUS/AVI
./libvlc_demux_fuzzer -jobs=$jobs DEMUX_CORPUS/FLV
./libvlc_demux_fuzzer -jobs=$jobs DEMUX_CORPUS/MKV
./libvlc_demux_fuzzer -jobs=$jobs DEMUX_CORPUS/MP4 -dict=dictionaries/mp4.dict
./libvlc_demux_fuzzer -jobs=$jobs DEMUX_CORPUS/MPEG1
./libvlc_demux_fuzzer -jobs=$jobs DEMUX_CORPUS/MPEG2
./libvlc_demux_fuzzer -jobs=$jobs DEMUX_CORPUS/OPUS

./libvlc_decoder_fuzzer -jobs=$jobs --codec=H264 DECODER_CORPUS/H264
./libvlc_decoder_fuzzer -jobs=$jobs --codec=H265 DECODER_CORPUS/H265
./libvlc_decoder_fuzzer -jobs=$jobs --codec=MP2V DECODER_CORPUS/MP2V
./libvlc_decoder_fuzzer -jobs=$jobs --codec=MP4V DECODER_CORPUS/MP4V
./libvlc_decoder_fuzzer -jobs=$jobs --codec=VC-1 DECODER_CORPUS/VC-1
