if test -z "$1"; then
	echo "Usage: $0 <fuzzer target>"
	echo "Example: $0 libvlc_demux_fuzzer"
	exit 1
fi

fuzzer=$1
workers=1

CXXFLAGS="-O1 -fno-omit-frame-pointer -fsanitize=address -fsanitize-address-use-after-scope -fsanitize-coverage=trace-pc -static-libstdc++"
LD_LIBRARY_PATH=../../src/.libs/:../../lib/.libs

g++ \
    ${CXXFLAGS} -I../../include/ -I../../include/vlc \
    ${fuzzer}.cpp -o ${fuzzer} \
    -L../../src/.libs/ -lvlccore -L../../lib/.libs -lvlc -L./ -lFuzzer \
    -std=c++11 -lpthread

export LD_LIBRARY_PATH
export ASAN_SYMBOLIZER_PATH=/usr/bin/llvm-symbolizer

./${fuzzer} -workers=$workers DEMUX_CORPUS

exit
